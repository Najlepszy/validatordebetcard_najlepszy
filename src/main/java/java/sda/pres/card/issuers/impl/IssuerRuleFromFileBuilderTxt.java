package java.sda.pres.card.issuers.impl;


import java.io.BufferedReader;
import java.io.FileReader;
import java.sda.pres.card.issuers.IRuleBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IssuerRuleFromFileBuilderTxt implements IRuleBuilder {

    private static final String COMMENT_PREFIX = "#";
    private static final String RULE_SEPARATOR = ";";

    private String filePath;

    public IssuerRuleFromFileBuilderTxt(String filePath) {
        this.filePath = filePath;
    }


    @Override
    public List<Map<String, String>> buildRules() {
        //   List<IssuerRule> result = new ArrayList<>();
        List<Map<String, String>> result = new ArrayList<>();

        try {
            FileReader reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = "";
            while (((line = bufferedReader.readLine()) != null)) {
                if (line.startsWith(COMMENT_PREFIX)) {
                    continue;
                }
                String[] tokens = line.split(RULE_SEPARATOR);
                //   IssuerRule issuerRule = new IssuerRule();
                Map<String, String> issuerRuleMap = new HashMap<>();
                issuerRuleMap.put("name", tokens[0]);
                issuerRuleMap.put("prefix", tokens[1]);
                issuerRuleMap.put("lengh", tokens[2]);
                //issuerRule.setIssuerName(tokens[0]);
                // issuerRuleMap.put(ListaKluczy<>, tokens [0]);
                //issuerRule.setPrefix(Integer.parseInt(tokens[1]));
                // issuerRule.setLength(Integer.parseInt(tokens[2]));
                result.add(issuerRuleMap);
            }
        } catch (Exception e) {
            //TODO: throw exception
        }

        return result;
    }
}
